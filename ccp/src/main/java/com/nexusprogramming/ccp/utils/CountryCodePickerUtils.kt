package com.nexusprogramming.ccp.utils

import android.content.Context
import android.telephony.TelephonyManager
import androidx.compose.ui.text.intl.Locale
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import com.nexusprogramming.ccp.data.countryData.country.getLibCountries
import com.nexusprogramming.ccp.data.model.CountryData
import com.nexusprogramming.ccp.data.model.PhoneNumberData

internal fun getDefaultCountryData(context: Context): CountryData {
    val countryNameCode = getDefaultLangCode(context)
    return getLibCountries.first { it.countryNameCode == countryNameCode }
}

internal fun getDefaultCountryData(context: Context, defaultCountyName: String): CountryData {
    return if (defaultCountyName.isBlank()) {
        getLibCountries.first { it.countryNameCode == getDefaultLangCode(context) }
    } else {
        getLibCountries.first { it.countryName == defaultCountyName }
    }
}

internal fun getDefaultCountryName(context: Context): String {
    val defaultCountry = getDefaultLangCode(context)
    val countryData = getLibCountries.first { it.countryNameCode == defaultCountry }
    return countryData.countryName
}

internal fun getDefaultLangCode(context: Context): String {
    val localeCode: TelephonyManager =
        context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    val countryCode = localeCode.networkCountryIso
    val defaultLocale = Locale.current.language
    return countryCode.ifBlank { defaultLocale }
}

internal fun getDefaultPhoneCode(context: Context): String {
    val defaultCountry = getDefaultLangCode(context)
    val defaultCode: CountryData = getLibCountries.first { it.countryNameCode == defaultCountry }
    return defaultCode.countryPhoneCode.ifBlank { "+221" }
}

fun PhoneNumberData.isValid(): Boolean {
    if (number.isNotBlank() && number.length > 6 && countryCode.isNotBlank()) {
        return checkPhoneNumber(
            fullPhoneNumber = countryCode + number,
            countryCode = countryCode
        )
    }
    return false
}

private fun checkPhoneNumber(
    fullPhoneNumber: String,
    countryCode: String
): Boolean {
    val number: Phonenumber.PhoneNumber?
    return try {
        number = PhoneNumberUtil.getInstance().parse(
            fullPhoneNumber,
            Phonenumber.PhoneNumber.CountryCodeSource.UNSPECIFIED.name
        )
        !PhoneNumberUtil.getInstance().isValidNumberForRegion(number, countryCode.uppercase())
    } catch (ex: Exception) {
        true
    }
}
