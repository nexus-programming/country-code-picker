package com.nexusprogramming.ccp.utils

import com.nexusprogramming.ccp.data.countryData.country.getLibCountries
import com.nexusprogramming.ccp.data.model.CountryData

internal fun String.getCountryData(): CountryData? {
    var countryData: CountryData? = null
    getLibCountries.forEach {
        if (it.countryNameCode.equals(this, ignoreCase = true)) {
            countryData = it
            return@forEach
        }
    }
    return countryData
}

internal fun List<String>.searchString(key: String): List<String> {
    val tempList = mutableListOf<String>()
    this.forEach { state ->
        if (state.lowercase().contains(key.lowercase())) {
            tempList.add(state)
        }
    }
    return tempList
}
