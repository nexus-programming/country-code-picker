package com.nexusprogramming.ccp.utils

import android.content.Context
import com.nexusprogramming.ccp.data.countryData.country.getStringIdCountryName
import com.nexusprogramming.ccp.data.model.CountryData

internal fun List<CountryData>.searchCountry(key: String, context: Context): List<CountryData> {
    val tempList = mutableListOf<CountryData>()
    this.forEach { countryData ->
        val value = context.resources.getString(getStringIdCountryName(countryData.countryNameCode))
        if (value.lowercase().contains(key.lowercase())) {
            tempList.add(countryData)
        }
    }
    return tempList
}
