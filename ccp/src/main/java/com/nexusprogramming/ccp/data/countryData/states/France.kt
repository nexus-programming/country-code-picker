package com.nexusprogramming.ccp.data.countryData.states

internal val franceRegions = listOf(
    "Alsace",
    "Aquitaine",
    "Auvergne",
    "Bretagne",
    "Bourgogne",
    "Centre",
    "Champagne-Ardenne",
    "Corse",
    "Franche-Comté",
    "Languedoc-Roussillon",
    "Limousin",
    "Lorraine",
    "Basse-Normandie",
    "Midi-Pyrénées",
    "Nord-Pas-de-Calais",
    "Île-de-France",
    "Pays-de-la-Loire",
    "Picardie",
    "Poitou-Charentes",
    "Provence-Alpes-Côte d'Azur",
    "Rhône-Alpes",
    "Haute-Normandie"
)
