package com.nexusprogramming.ccp.data.model

import com.nexusprogramming.ccp.R
import java.util.Locale

data class CountryData(
    private var _countryNameCode: String,
    val countryPhoneCode: String = "+221",
    val countryName: String = "Senegal",
    val states: List<String> = emptyList(),
    val flagResID: Int = R.drawable.sn,
) {
    val countryNameCode = _countryNameCode.lowercase(Locale.getDefault())
}
