package com.nexusprogramming.ccp.data.countryData.states

internal val senegalRegions = listOf(
    "Dakar",
    "Diourbel",
    "Fatick",
    "Kaolack",
    "Kolda",
    "Louga",
    "Matam",
    "Saint-Louis",
    "Tambacounda",
    "Thiès",
    "Ziguinchor",
    "Sédhiou",
    "Kaffrine",
    "Kédougou"
)
