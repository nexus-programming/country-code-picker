package com.nexusprogramming.ccp.data.model

data class PhoneNumberData(
    val number: String = "",
    val countryCode: String = "",
    val languageCode: String = "",
    val fullPhoneNumber: String = ""
)
