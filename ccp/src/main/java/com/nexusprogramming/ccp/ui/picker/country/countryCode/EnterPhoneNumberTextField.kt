package com.nexusprogramming.ccp.ui.picker.country.countryCode

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalTextInputService
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.nexusprogramming.ccp.R
import com.nexusprogramming.ccp.data.countryData.country.getLibCountries
import com.nexusprogramming.ccp.data.countryData.country.getNumberHint
import com.nexusprogramming.ccp.data.model.PhoneNumberData
import com.nexusprogramming.ccp.transformation.PhoneNumberTransformation
import com.nexusprogramming.ccp.ui.component.SimpleTextInput
import com.nexusprogramming.ccp.utils.getDefaultLangCode
import com.nexusprogramming.ccp.utils.getDefaultPhoneCode

private val defaultOutlinedTextFieldColors
    @Composable get() = OutlinedTextFieldDefaults.colors(
        focusedTextColor = MaterialTheme.colorScheme.primary,
        unfocusedTextColor = MaterialTheme.colorScheme.primary,
        focusedBorderColor = MaterialTheme.colorScheme.primaryContainer,
        unfocusedBorderColor = MaterialTheme.colorScheme.primaryContainer,
    )

/**
 * Get country code for phone number in sign in screen
 */
@Composable
fun EnterPhoneNumberTextField(
    modifier: Modifier = Modifier,
    label: String,
    defaultValue: String = "",
    isRequired: Boolean = true,
    textFieldColors: TextFieldColors = defaultOutlinedTextFieldColors,
    textStyle: TextStyle = MaterialTheme.typography.bodyLarge,
    onValueChanged: (PhoneNumberData) -> Unit
) {

    val context = LocalContext.current
    val keyboardController = LocalTextInputService.current
    var textFieldValue by rememberSaveable { mutableStateOf(defaultValue) }

    val labelSuffix = if (isRequired) {
        stringResource(id = R.string.require_symbol_label)
    } else {
        stringResource(id = R.string.optional_label)
    }

    val labelSuffixColor = if (isRequired) {
        MaterialTheme.colorScheme.error
    } else {
        MaterialTheme.colorScheme.primary.copy(alpha = 0.5f)
    }

    var countryPhoneCode by remember { mutableStateOf(getDefaultPhoneCode(context)) }
    var countryLanguageCode by remember { mutableStateOf(getDefaultLangCode(context)) }

    Row(
        modifier = modifier.padding(horizontal = 16.dp),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        OutlinedTextField(
            modifier = Modifier.fillMaxSize(),
            value = textFieldValue,
            onValueChange = { inputNumber ->
                textFieldValue = inputNumber
                onValueChanged(
                    PhoneNumberData(
                        number = inputNumber,
                        countryCode = countryPhoneCode,
                        languageCode = countryLanguageCode,
                        fullPhoneNumber = "$countryPhoneCode$inputNumber"
                    )
                )
            },
            singleLine = true,
            colors = textFieldColors,
            visualTransformation = PhoneNumberTransformation(
                countryCode = getLibCountries.single { it.countryNameCode == countryLanguageCode }.countryNameCode.uppercase()
            ),
            label = {
                Row {
                    SimpleTextInput(
                        text = label,
                        style = textStyle,
                        color = MaterialTheme.colorScheme.primary.copy(alpha = 0.5f)
                    )
                    SimpleTextInput(
                        modifier = Modifier.padding(start = 5.dp),
                        text = labelSuffix,
                        color = labelSuffixColor,
                        style = textStyle
                    )
                }
            },
            placeholder = {
                Text(
                    text = stringResource(id = getNumberHint(getLibCountries.single { it.countryNameCode == countryLanguageCode }.countryNameCode.lowercase())),
                    style = textStyle,
                    color = Color.Black.copy(alpha = 0.5f)
                )
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Phone,
                imeAction = ImeAction.Done,
                autoCorrect = false,
            ),
            keyboardActions = KeyboardActions(onDone = {
                keyboardController?.hideSoftwareKeyboard()
            }),
            leadingIcon = {
                Row {
                    Column {
                        CountryCodePicker(
                            textStyle = textStyle,
                            onSelectedCountryData = {
                                countryPhoneCode = it.countryPhoneCode
                                countryLanguageCode = it.countryNameCode
                            },
                            defaultSelectedCountry = getLibCountries.single { it.countryNameCode == countryLanguageCode }
                        )
                    }
                }
            },
            textStyle = textStyle,
        )
    }
}
