package com.nexusprogramming.ccp.ui.picker.country.countryName

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.Interaction
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.PressInteraction
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.nexusprogramming.ccp.R
import com.nexusprogramming.ccp.data.countryData.country.getFlags
import com.nexusprogramming.ccp.data.countryData.country.getLibCountries
import com.nexusprogramming.ccp.data.model.CountryData
import com.nexusprogramming.ccp.ui.component.SimpleTextInput
import com.nexusprogramming.ccp.ui.picker.country.CountryDialog
import com.nexusprogramming.ccp.utils.getDefaultCountryData
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow

private val defaultOutlinedTextFieldColors
    @Composable get() = OutlinedTextFieldDefaults.colors(
        focusedTextColor = MaterialTheme.colorScheme.primary,
        unfocusedTextColor = MaterialTheme.colorScheme.primary,
        focusedBorderColor = MaterialTheme.colorScheme.primaryContainer,
        unfocusedBorderColor = MaterialTheme.colorScheme.primaryContainer,
    )

/**
 * Get country code for phone number in sign in screen
 */
@Composable
fun SelectCountryTextField(
    modifier: Modifier = Modifier,
    defaultCountryName: String = "",
    isRequired: Boolean = true,
    onCountrySelected: (String) -> Unit
) {

    val context = LocalContext.current

    val countryList: List<CountryData> = getLibCountries
    var shouldOpenDialog by remember { mutableStateOf(false) }
    var countryData by remember {
        mutableStateOf(getDefaultCountryData(context, defaultCountryName))
    }

    val interactionSource = remember {
        object : MutableInteractionSource {
            override val interactions = MutableSharedFlow<Interaction>(
                extraBufferCapacity = 16,
                onBufferOverflow = BufferOverflow.DROP_OLDEST,
            )

            override suspend fun emit(interaction: Interaction) {
                if (interaction is PressInteraction.Release) {
                    shouldOpenDialog = true
                }
                interactions.emit(interaction)
            }

            override fun tryEmit(interaction: Interaction): Boolean {
                return interactions.tryEmit(interaction)
            }
        }
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .clickable { shouldOpenDialog = true },
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        OutlinedTextField(
            modifier = modifier.fillMaxSize(),
            value = countryData.countryName,
            onValueChange = {},
            interactionSource = interactionSource,
            readOnly = true,
            singleLine = true,
            colors = defaultOutlinedTextFieldColors,
            textStyle = MaterialTheme.typography.bodyLarge,
            label = {
                Row {
                    SimpleTextInput(
                        text = stringResource(R.string.country),
                        color = Color.Black.copy(alpha = 0.5f)
                    )
                    if (isRequired) {
                        SimpleTextInput(
                            modifier = Modifier.padding(start = 5.dp),
                            text = stringResource(id = R.string.require_symbol_label),
                            color = Color.Red
                        )
                    }
                }
            },
            leadingIcon = {
                Image(
                    modifier = Modifier
                        .width(40.dp)
                        .padding(horizontal = 3.dp),
                    painter = painterResource(id = getFlags(countryData.countryNameCode)),
                    contentDescription = null
                )
            },
            trailingIcon = {
                Icon(
                    modifier = Modifier.padding(horizontal = 3.dp),
                    imageVector = Icons.Filled.ArrowDropDown,
                    contentDescription = null,
                    tint = Color.Black
                )
            }
        )
        if (shouldOpenDialog) {
            CountryDialog(
                countryList = countryList,
                onDismissRequest = { shouldOpenDialog = false },
                context = context,
                dialogStatus = shouldOpenDialog,
                onSelectedCountryData = { data ->
                    countryData = data
                    onCountrySelected(data.countryName)
                    shouldOpenDialog = false
                }
            )
        }
    }
}
