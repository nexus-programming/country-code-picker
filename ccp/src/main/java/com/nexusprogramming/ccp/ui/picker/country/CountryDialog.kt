package com.nexusprogramming.ccp.ui.picker.country

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.nexusprogramming.ccp.data.countryData.country.getFlags
import com.nexusprogramming.ccp.data.countryData.country.getStringIdCountryName
import com.nexusprogramming.ccp.data.model.CountryData
import com.nexusprogramming.ccp.ui.component.SearchTextField
import com.nexusprogramming.ccp.utils.searchCountry

@Composable
internal fun CountryDialog(
    modifier: Modifier = Modifier,
    context: Context,
    countryList: List<CountryData>,
    textStyle: TextStyle = MaterialTheme.typography.bodyLarge,
    dialogStatus: Boolean,
    onDismissRequest: () -> Unit,
    onSelectedCountryData: (item: CountryData) -> Unit
) {

    var searchValue by remember { mutableStateOf("") }
    if (!dialogStatus) searchValue = ""

    Dialog(
        onDismissRequest = onDismissRequest,
        content = {
            Surface(
                color = MaterialTheme.colorScheme.onBackground,
                modifier = Modifier
                    .background(MaterialTheme.colorScheme.background)
                    .fillMaxWidth()
                    .padding(vertical = 50.dp, horizontal = 20.dp)
                    .clip(RoundedCornerShape(25.dp))
            ) {
                Scaffold { scaffold ->
                    scaffold.calculateBottomPadding()
                    Column(modifier = Modifier.fillMaxSize()) {
                        SearchTextField(
                            textStyle = textStyle,
                            onValueChange = { searchValue = it }
                        )
                        Spacer(modifier = Modifier.height(10.dp))

                        LazyColumn {
                            items(
                                if (searchValue.isEmpty()) {
                                    countryList
                                } else {
                                    countryList.searchCountry(searchValue, context)
                                }
                            ) { countryItem ->
                                Row(
                                    Modifier
                                        .padding(18.dp)
                                        .fillMaxWidth()
                                        .clickable(onClick = { onSelectedCountryData(countryItem) }),
                                    horizontalArrangement = Arrangement.Start,
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    Image(
                                        modifier = modifier.width(30.dp),
                                        painter = painterResource(id = getFlags(countryItem.countryNameCode)),
                                        contentDescription = null
                                    )
                                    Text(
                                        text = stringResource(
                                            id = getStringIdCountryName(
                                                countryItem.countryNameCode.lowercase()
                                            )
                                        ),
                                        modifier = Modifier.padding(horizontal = 18.dp),
                                        style = textStyle,
                                        color = Color.Black
                                    )
                                }
                            }
                        }
                    }
                }
            }
        },
    )
}
