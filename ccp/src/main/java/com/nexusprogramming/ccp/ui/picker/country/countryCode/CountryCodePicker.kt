package com.nexusprogramming.ccp.ui.picker.country.countryCode

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import com.nexusprogramming.ccp.data.countryData.country.getFlags
import com.nexusprogramming.ccp.data.countryData.country.getLibCountries
import com.nexusprogramming.ccp.data.model.CountryData
import com.nexusprogramming.ccp.ui.picker.country.CountryDialog

@Composable
internal fun CountryCodePicker(
    modifier: Modifier = Modifier,
    textStyle: TextStyle = MaterialTheme.typography.bodyLarge,
    onSelectedCountryData: (CountryData) -> Unit,
    defaultSelectedCountry: CountryData = getLibCountries.first(),
) {

    val context = LocalContext.current

    val countryList: List<CountryData> = getLibCountries
    var isOpenDialog by remember { mutableStateOf(false) }
    val interactionSource = remember { MutableInteractionSource() }
    var selectedCountryData by remember { mutableStateOf(defaultSelectedCountry) }

    Column(
        modifier = modifier
            .padding(15.dp)
            .clickable(
                interactionSource = interactionSource,
                indication = null,
            ) {
                isOpenDialog = true
            }
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                contentDescription = null,
                modifier = modifier.width(40.dp),
                painter = painterResource(id = getFlags(selectedCountryData.countryNameCode)),
            )
            Text(
                text = selectedCountryData.countryPhoneCode,
                modifier = Modifier.padding(start = 6.dp),
                color = Color.Black,
                style = textStyle
            )
            Icon(imageVector = Icons.Default.ArrowDropDown, contentDescription = null)
        }

        if (isOpenDialog) {
            CountryDialog(
                context = context,
                countryList = countryList,
                onDismissRequest = { isOpenDialog = false },
                textStyle = textStyle,
                dialogStatus = isOpenDialog,
                onSelectedCountryData = { countryItem ->
                    onSelectedCountryData(countryItem)
                    selectedCountryData = countryItem
                    isOpenDialog = false
                }
            )
        }
    }
}
