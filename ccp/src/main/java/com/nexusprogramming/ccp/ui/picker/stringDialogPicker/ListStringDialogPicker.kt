package com.nexusprogramming.ccp.ui.picker.stringDialogPicker

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.nexusprogramming.ccp.R
import com.nexusprogramming.ccp.ui.component.SimpleTextInput

@Composable
fun ListStringDialogPicker(
    modifier: Modifier = Modifier,
    label: String,
    listString: List<String>,
    onSelect: (String) -> Unit
) {

    onSelect(listString.first())

    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
    ) {
        Spacer(modifier = Modifier.height(8.dp))
        Row(
            modifier = Modifier.padding(horizontal = 16.dp),
            horizontalArrangement = Arrangement.Start
        ) {
            SimpleTextInput(
                text = label,
                color = Color.Black,
                style = MaterialTheme.typography.bodyMedium
            )
            SimpleTextInput(
                modifier = Modifier.padding(start = 5.dp),
                text = stringResource(id = R.string.require_symbol_label),
                color = MaterialTheme.colorScheme.error,
                style = MaterialTheme.typography.bodyMedium
            )
        }
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            StringDialogView(
                listStrings = listString,
                onSelect = {
                    onSelect(it)
                }
            )
        }
        Spacer(modifier = Modifier.height(8.dp))
    }
}
