package com.nexusprogramming.ccp.ui.picker.stringDialogPicker

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
internal fun StringDialogView(
    modifier: Modifier = Modifier,
    listStrings: List<String>,
    onSelect: (String) -> Unit
) {

    var shouldOpenDialog by remember { mutableStateOf(false) }
    val interactionSource = remember { MutableInteractionSource() }
    var selectedState by remember { mutableStateOf(listStrings.first()) }

    Row(
        modifier = modifier
            .wrapContentWidth()
            .clickable(
                interactionSource = interactionSource,
                indication = null,
            ) {
                shouldOpenDialog = true
            },
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            modifier = Modifier.padding(start = 10.dp),
            text = selectedState,
            color = Color.Black,
            style = MaterialTheme.typography.bodyMedium
        )
        Icon(imageVector = Icons.Default.ArrowDropDown, contentDescription = null)
    }

    if (shouldOpenDialog) {
        StatesDialog(
            listStates = listStrings,
            onDismissRequest = { shouldOpenDialog = false },
            dialogStatus = shouldOpenDialog,
            onStateSelected = { text ->
                onSelect(text)
                selectedState = text
                shouldOpenDialog = false
            }
        )
    }
}
